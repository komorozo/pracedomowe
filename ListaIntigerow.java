package com.sda.zadanieDomowe3;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ListaIntigerow {

    public static void main(String[] args) {

        List<Integer> listaIntigerow = new ArrayList<>(100);


        Random random = new Random();
        for (int i = 0; i < 100; i++) {
            listaIntigerow.add(random.nextInt(200));
        }

        int max = 0;
        for (int i = 0; i < listaIntigerow.size(); i++) {
            if (listaIntigerow.get(i) > max) {
                max = listaIntigerow.get(i);
            }
        }

        int suma = 0;
        for (Integer liczba : listaIntigerow) {
            suma = suma + liczba;
        }
        double sredniaListy = (suma / listaIntigerow.size());

        double srednia = sredniaListy;


        System.out.println("-----------LISTA 100 INTIGEROW------------");
        System.out.println(listaIntigerow);
        System.out.println("-----------MAX------------");
        System.out.println(max);
        System.out.println("-----------SUMA------------");
        System.out.println(suma);
        System.out.println("-----------SREDNIA------------");
        System.out.println(sredniaListy);
        System.out.println("-----------PO USUNIECIU LICZB WIEKSZYCH OD SREDNIEJ------------");


        for (int i = listaIntigerow.size()-1; i >= 0 ; i--) {
            if (listaIntigerow.get(i)>sredniaListy){
                listaIntigerow.remove(i);
            }
        }
        System.out.println(listaIntigerow);



        int sumaPoUsunieciuElementow = 0;
        for (Integer liczba : listaIntigerow){
            sumaPoUsunieciuElementow += liczba;
        }
        System.out.println("-----------SUMA LISTY PO USUNIECIU LICZB WIEKSZYCH OD SREDNIEJ------------");

        System.out.println(sumaPoUsunieciuElementow);
        double sredniaPoUsunieciElementow = sumaPoUsunieciuElementow/listaIntigerow.size();
        System.out.println("-----------SREDNIA LISTY PO USUNIECIU LICZB WIEKSZYCH OD SREDNIEJ------------");
        System.out.println(sredniaPoUsunieciElementow);


    }
}
