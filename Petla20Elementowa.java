package com.sda.zadanieDomowe3;

import java.util.Random;

public class Petla20Elementowa {
    public static void main(String[] args) {

        int[] tablica = new int[20];
        Random random = new Random();
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(100);
        }

        int max = 0;
        for (int i = 0; i < tablica.length - 1; i++) {

            if (tablica[i] > max) {
                max = tablica[i];
            }
            System.out.print(tablica[i] + " ");
        }
        System.out.println();
        System.out.println(max);

        int suma=0;
        for (int i = 0; i <tablica.length ; i++) {
            suma = suma + tablica[i];
        }
        double srednia = suma/tablica.length;
        System.out.println(suma);
        System.out.println(srednia);
    }


}
