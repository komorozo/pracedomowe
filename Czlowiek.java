package com.sda.zadanieDomowe3;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Czlowiek implements Comparator<Czlowiek>{
    int id;
    String imie;
    String nazwisko;
    String login;
    String plec;


    public Czlowiek(int id, String imie, String nazwisko, String login, String plec) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.login = login;
        this.plec = plec;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }



    @Override
    public int compare(Czlowiek o1, Czlowiek o2) {
        if (o1.getNazwisko().compareToIgnoreCase(o2.getNazwisko())>0){
            return -1;
        }
        if (o1.getNazwisko().compareToIgnoreCase(o2.getNazwisko())<0){
            return 1;
        }else
            return 0;


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Czlowiek czlowiek = (Czlowiek) o;

        if (id != czlowiek.id) return false;
        if (imie != null ? !imie.equals(czlowiek.imie) : czlowiek.imie != null) return false;
        if (nazwisko != null ? !nazwisko.equals(czlowiek.nazwisko) : czlowiek.nazwisko != null) return false;
        if (login != null ? !login.equals(czlowiek.login) : czlowiek.login != null) return false;
        return plec != null ? plec.equals(czlowiek.plec) : czlowiek.plec == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (imie != null ? imie.hashCode() : 0);
        result = 31 * result + (nazwisko != null ? nazwisko.hashCode() : 0);
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (plec != null ? plec.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Czlowiek{" +
                "id=" + id +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", login='" + login + '\'' +
                ", plec='" + plec + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Set<Czlowiek> zbiorLudzi = new TreeSet<>();

        Czlowiek c1 = new Czlowiek(1,"lukasz", "komorowski", "komorozo","m");
        Czlowiek c2 = new Czlowiek(2,"marek", "kowalski", "kowmar","m");
        Czlowiek c3 = new Czlowiek(3,"bogdan", "komorowski", "kombog","m");
        Czlowiek c4 = new Czlowiek(4,"marcin", "stonoga", "stomar","m");
        Czlowiek c5 = new Czlowiek(5,"maja", "komorowska", "majkom","k");
        Czlowiek c6 = new Czlowiek(6,"patrycja", "komorowska", "kompat","k");
        Czlowiek c7 = new Czlowiek(7,"lukasz", "nowak", "noluk","m");
        Czlowiek c8 = new Czlowiek(8,"robert", "janowski", "robjan","m");
        Czlowiek c9 = new Czlowiek(9,"ania", "stasiak", "stania","k");
        Czlowiek c10 = new Czlowiek(10,"krysia", "nowak", "kryno","k");

        zbiorLudzi.add(c1);
        zbiorLudzi.add(c2);
        zbiorLudzi.add(c3);
        zbiorLudzi.add(c4);
        zbiorLudzi.add(c5);
        zbiorLudzi.add(c6);
        zbiorLudzi.add(c7);
        zbiorLudzi.add(c8);
        zbiorLudzi.add(c9);
        zbiorLudzi.add(c10);

        for (Czlowiek czlowiek : zbiorLudzi){
            System.out.println(czlowiek);
        }
    }
}
